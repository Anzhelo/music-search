import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import '../css/main.scss';

import { store } from './redux/createStore';

import App from './components/App';

const connectedApp = (
  <Provider store={store}>
    <App />
  </Provider>
);

ReactDOM.render(connectedApp, document.querySelector('#reactContainer'));