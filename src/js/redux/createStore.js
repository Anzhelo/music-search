import { createStore, combineReducers } from 'redux';

import loginReducer from './reducers/loginReducer';
import searchReducer from './reducers/searchReducer';

const combinedReducers = combineReducers({
  login: loginReducer,
  search: searchReducer
});

export const store = createStore(combinedReducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());