import { LOGINUSER, LOGINERROR } from '../actions/loginUserAction';
import { LOGOUTUSER } from '../actions/logoutUserAction';

const defaultState = {
  loggedIn: false,
  username: null,
  loginError: null
}

const loginReducer = (state = defaultState, action) => {
  switch(action.type) {
    case LOGINUSER:
      return {
        ...state,
        loggedIn: true,
        username: action.payload.username,
        loginError: null
      };

    case LOGINERROR:
      return {
        ...state,
        loggedIn: false,
        username: null,
        loginError: 'Incorrect username and/or password'
      }

    case LOGOUTUSER:
      return {
        ...state,
        loggedIn: false,
        username: null,
        loginError: null
      };

    default:
      return state;
  }
};

export default loginReducer;