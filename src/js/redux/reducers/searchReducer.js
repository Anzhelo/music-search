import { SEARCHMUSICIAN } from '../actions/searchMusicianAction';
import { CLEARSEARCH } from '../actions/clearSearchAction';
import { DELETEMUSICIAN } from '../actions/deleteMusicianAction';

const defaultState = {
  searchTerm: '',
  foundMusicians: []
};

const searchReducer = (state = defaultState, action) => {
  switch(action.type) {
    case SEARCHMUSICIAN:
      const { searchTerm, results } = action.payload;
      return {
        ...state,
        searchTerm,
        foundMusicians: results
      };

      case DELETEMUSICIAN:
        return{
            ...state,
            searchTerm: state.searchTerm,
            foundMusicians: state.foundMusicians.filter(musician => musician.msId !== action.payload.id)
        };
    case CLEARSEARCH:
      return {
        ...state,
        searchTerm: '',
        foundMusicians: []
      };

    default:
      return state;
  }
};

export default searchReducer;