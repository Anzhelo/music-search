import _ from 'lodash';
import uuid from 'uuid';

export const SEARCHMUSICIAN = 'search/SEARCHMUSICIAN';

export const searchMusician = (searchTerm, results) => {
  const refinedResults = _.take(
                           _.orderBy(
                             _.filter(results, result => result.mbid), 
                           result => parseInt(result.listeners), 'desc')
                         , 10);

  const enhancedResults = refinedResults.map(r => {
    return { 
      ...r,
      msId: uuid.v4()
    };
  });

  return {
    type: SEARCHMUSICIAN,
    payload: {
      searchTerm,
      results: enhancedResults
    }
  };
};