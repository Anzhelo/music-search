export const DELETEMUSICIAN = 'search/DELETEMUSICIAN';

export const deleteMusician = (id) => {
return {
    type: DELETEMUSICIAN,
    payload: {
        id: id
    }
}};