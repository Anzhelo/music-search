import React from 'react';
import { connect } from 'react-redux';

import Result from './Result';

class Results extends React.Component {
  constructor() {
    super();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(this.props.results.length !== nextProps.results.length) return true;
    for(let i = 0; i < this.props.results.length; i++) {
      if(this.props.results[i].msId !== nextProps.results[i].msId) return true;
    }

    return false;
  }

  render() {
    return (
      <div className="results">
        {this.renderResults()}
      </div>
    );
  }

  renderResults() {
    if(this.props.results.length) {
      return (
        this.props.results.map((result, index) => {
          return <Result key={result.msId} index={(index + 1)} musician={result} />
        })
      );
    }
    else if(this.props.searchTerm) {
      return <p className="results-message">Could not find any results</p>
    }
    else {
      return <p className="results-message">Search for a musician to get started</p>
    }
  }
}

const mapStateToProps = state => ({
  searchTerm: state.search.searchTerm,
  results: state.search.foundMusicians
});

export default connect(mapStateToProps)(Results);