import React from 'react';
import PropTypes from 'prop-types';
import {store} from '../redux/createStore';
import {deleteMusician} from "../redux/actions/deleteMusicianAction";

class Result extends React.PureComponent {
  constructor() {
    super();
    this.onArtistDeleteHandler = this.onArtistDeleteHandler.bind(this);
  }

  render() {
    return (
      <div className="musician">
      
        <a className="musician-link"
           href={this.props.musician.url}
           target="_blank">

          <h3 className="musician-link-title">
            <span className="musician-link-title-index">
              {this.props.index}.&nbsp;
            </span>
            <span className="musician-link-title-name">
              {this.props.musician.name}
            </span>
          </h3>
          
          <div className="musician-link-info">
            <p className="musician-link-info-listeners">
              <span>
                {this.props.musician.listeners}
              </span>
              &nbsp;listeners
            </p>
          </div>
          
          <img src={this.props.musician.image[2]['#text']} />
        </a>

        {
          /* TODO: IMPLEMENT REMOVE FUNCTIONALITY */
        }
        <button className="musician-remove" onClick={this.onArtistDeleteHandler}>remove</button>
      </div>
    );
  }

  onArtistDeleteHandler(){
      store.dispatch(deleteMusician(this.props.musician.msId));
  }
}

Result.propTypes = {
  index: PropTypes.number.isRequired,
  musician: PropTypes.object.isRequired
};

export default Result;
