import React from 'react';
import { connect } from 'react-redux';

import { logoutUser } from '../redux/actions/logoutUserAction';
import { store } from '../redux/createStore';
import { clearSearch } from '../redux/actions/clearSearchAction';

class Header extends React.Component {
  constructor() {
    super();

    this.onLogoutClickHandler = this.onLogoutClickHandler.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextProps.loggedIn !== this.props.loggedIn) return true;
    if(nextProps.username !== this.props.username) return true;
    
    return false;
  }

  renderHeaderContent () {
    if(this.props.loggedIn)
      return (
        <div className="header-credentials">
          <p className="header-credentials-welcomebackmessage">Welcome back,&nbsp; 
            <span id="header-credentials-username">{this.props.username}</span>
          </p>
          <p className="header-credentials-logout" onClick={this.onLogoutClickHandler}>Log out</p>
        </div>
      );
    else
      return (
        <p>Please log in to enjoy all features...</p>
      );
  }

  render() {
    return (
      <div className="header">
        <h1>Music-search</h1>
        {this.renderHeaderContent()}
      </div>
    );  
  }

  onLogoutClickHandler() {
    store.dispatch(clearSearch());
    store.dispatch(logoutUser());
  }
}

const mapStateToProps = (state) => ({
  loggedIn: state.login.loggedIn,
  username: state.login.username
});

export default connect(mapStateToProps)(Header);